﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class UI_SpitBean : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	public static bool s_bUsingUi = false;

	bool m_bSplitting = false;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () 
    {

	}



    public void OnPointerDown(PointerEventData evt)
	{
		if (this.gameObject.name == "btnSpitSpore") { 
			if (!Main.s_Instance.m_MainPlayer.IsSpittingSpore ()) {
				Main.s_Instance.m_MainPlayer.BeginSpitSpore ();
			}
		}
    }

    public void OnPointerUp(PointerEventData evt)
    {
		if (this.gameObject.name == "btnSpitSpore") {	
			if (Main.s_Instance.m_MainPlayer.IsSpittingSpore ()) {
				Main.s_Instance.m_MainPlayer.EndSpitSpore();
			}
		}



    }

	
}
