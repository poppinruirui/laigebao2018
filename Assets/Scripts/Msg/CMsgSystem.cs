﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CMsgSystem : MonoBehaviour {

	public static CMsgSystem s_Instance;

	public Text[] _aryMsg;


    public enum eSysMsgType
    {
        mp_not_enough,             // 蓝不够
        not_use,                   // 这一条留空
        money_not_enough,          // 钱不够
        level_up, // 升级了
        cold_down_not_completed,   // ColdDown还没结束
        skill_not_activate,        // 技能还未加点
        reach_max_ball_num,        // 分球数已达上限
        not_enough_volume_to_Spit_spore, // 吐孢子体积不够
    };

    public string[] m_aryMsgContent;

	void Awake()
	{
		s_Instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowMsg( int nIdx, string szContent )
	{
		_aryMsg [nIdx].text = szContent;
	}

    public string GetMsgContentByType( eSysMsgType type )
    {
        int nIndex = (int)type;
        if ( nIndex >= m_aryMsgContent.Length )
        {
            return "";
        }

        return m_aryMsgContent[nIndex];
        /*
        string szContent = "";

        switch( type )
        {
            case eSysMsgType.mp_not_enough:
                {
                    szContent = "您没有足够的蓝释放该技能";
                }
                break;
            case eSysMsgType.cold_down_not_completed:
                {
                    szContent = "该技能还没有准备好";
                }
                break;
            case eSysMsgType.money_not_enough:
                {
                    szContent = "您没有足够的金币购买该技能";
                }
                break;
        }
        
        return szContent;
        */
    }
}
