using UnityEngine;
using UnityEngine.UI;

public class OutlineText
{
    public GameObject textGraphics;
    public Canvas canvas;
    public Text text;
    public CircleOutline outline;

    public OutlineText(GameObject textGraphics)
    {
        this.textGraphics = textGraphics;
        GameObject canvasObject = this.textGraphics.transform.Find("Canvas").gameObject;
        this.canvas = canvasObject.GetComponent<Canvas>();
        GameObject textObject = canvasObject.transform.Find("Text").gameObject;
        this.text = textObject.GetComponent<Text>();
        this.outline = textObject.GetComponent<CircleOutline>();
    }
    public void setFontStyle(FontStyle fontStyle, Color color, TextAnchor alignment)
    {
        this.text.fontStyle = fontStyle;
        this.text.color = color;
        this.text.alignment = alignment;
    }
    public void setFontSize(float fontSize)
    {
        var scale = fontSize / 1.0f; // (this.text.font.fontSize / StageCtrl.PIXELS_PER_UNIT);
        this.text.rectTransform.localScale = new Vector2(scale, scale);
    }
    public void setText(string text)
    {
        this.text.text = text;
    }
    public void setTextLayer(int sortingLayerID, int sortingOrder)
    {
        this.canvas.sortingLayerID = sortingLayerID;
        this.canvas.sortingOrder = sortingOrder;
    }
};
