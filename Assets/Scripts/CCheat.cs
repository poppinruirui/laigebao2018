﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CCheat : MonoBehaviour {

    static Vector3 vecTempPos = new Vector3();

    public static CCheat s_Instance;

    public bool m_bActive = false;

    public GameObject _goPanel;

    public Toggle _toggleFixedRebornPos;

    /// <summary>
    /// / 击杀信息相关
    /// </summary>
    public InputField _inputJiShaTestScript;

    public Toggle _toggleXingNengCeShi;
  
    private void Awake()
    {
        s_Instance = this;
    }

    // Use this for initialization
    void Start () {
        InitJiShaInfo();

    }
	
	// Update is called once per frame
	void Update () {
        
        ShitTest();
    }

    public bool GetActive()
    {
        return m_bActive;
    }

    public void SetActive( bool bActive )
    {
        m_bActive = bActive;
        _goPanel.SetActive( bActive );
    }

    public void OnClick_Cheat()
    {
        SetActive( !GetActive() );
    }

    void InitJiShaInfo()
    {
     
    }

    List<int> lstAssist = new List<int>();
    public void OnClickButton_JiSha()
    {
        lstAssist.Clear();
        string[] aryAssist;
        /*
        if (_inputZhuGong.text.Length > 0)
        {
            aryAssist = _inputZhuGong.text.Split(',');
            for (int i = 0; i < aryAssist.Length; i++)
            {
                lstAssist.Add(int.Parse(aryAssist[i]));
            }
        }
        
        CJiShaInfo.s_Instance.PushOneJiShaSysMsgIntoQueue("啊啊啊", "不不不",
                                     ResourceManager.s_Instance.GetBallSpriteByPlayerId(1),
                                     ResourceManager.s_Instance.GetBallSpriteByPlayerId(2),
                                     ref lstAssist,
                                     true,
                                     Main.s_Instance.m_MainPlayer.GetDuoSha(),
                                     Main.s_Instance.m_MainPlayer.GetContinuousKillNum()
                                  );
                                  */


       CJiShaInfo.s_Instance.ParseAndExecCommands(_inputJiShaTestScript.text );

    }

    public bool GetXingNengCeShi()
    {
        return _toggleXingNengCeShi.isOn;
    }


    public void OnBtnClick_PerformanceTest()
    {
        Main.s_Instance.m_MainPlayer.ViolentlyGenerateBalls();
    }

    public void ViolentlyGenerateBalls()
    {
        m_nShitCount = 0;
    }

    public int m_nShitCount = 1000;
    int m_nShitSkinCount = 0;
    float m_fShitTimeCount = 0;
    int m_nShitSkinIndex = 0;
    public void ShitTest()
    {
        if (m_nShitCount > 500)
        {
            return;
        }

        m_fShitTimeCount += Time.deltaTime;
        if (m_fShitTimeCount <= 0.3f)
        {
            return;
        }
        m_fShitTimeCount = 0;

       
        for ( int i = 0; i < 5; i++ )
        {
            GenrateOneBall();
        }
    }

    void GenrateOneBall()
    {
        m_nShitSkinCount++;
        if (m_nShitSkinCount >= 32)
        {
            m_nShitSkinIndex++;
            m_nShitSkinCount = 0;
        }

        Ball ball = GameObject.Instantiate(Main.s_Instance.m_preBall).GetComponent<Ball>();
        ball.GenerateMeshShape(m_nShitSkinIndex);
        ball._Trigger.enabled = false;

        //ball.m_msStaticShell.gameObject.SetActive( false );

        vecTempPos.x = (float)UnityEngine.Random.Range(-500, 500);
        vecTempPos.y = (float)UnityEngine.Random.Range(-500, 500);
        ball.transform.position = vecTempPos;


        float fScale = (float)UnityEngine.Random.Range(1f, 8f);
        vecTempPos.x = fScale;
        vecTempPos.y = fScale;
        vecTempPos.z = 1f;
        ball.transform.localScale = vecTempPos;
        ball.SetSortingOrder(0);
        ball.SetPlayerName("测试球");
        ball.BeginAutoMove();
        m_nShitCount++;
    }

    public InputField _inputTRiangleNum;
    public  void OnClickButton_ChangeTriangleNum()
    {
        int nNum = 18;
        if ( !int.TryParse( _inputTRiangleNum.text, out nNum) )
        {
            nNum = 18;
        }
        if (nNum < 1 )
        {
            nNum = 18;
        }
        Main.s_Instance.m_MainPlayer.ChangeTriangleNum(nNum);
    }
}
