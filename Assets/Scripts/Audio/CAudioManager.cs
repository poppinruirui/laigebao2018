﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;



public class CAudioManager : MonoBehaviour {
    
    public static CAudioManager s_Instance;

    public AudioSource audio_main_bg;

    public AudioSource[] aryAudio;

    public enum eAudioId
    {
        e_audio_explode = 0,
        e_audio_spit,
        e_audio_unfold,
        e_audio_sneak,
        e_audio_levelup,
        e_audio_dead,
        e_audio_spore,
        e_audio_eat,
		e_audio_enter_game, // 进入游戏
		e_audio_flip_page,  // 翻页
		e_audio_mouse_click_button, // 点击

    };

    void Awake()
    {
        s_Instance = this;


    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PlayAudio( eAudioId id )
    {
        int nIndex = (int)id;
        if (aryAudio == null || nIndex >= aryAudio.Length || aryAudio[nIndex] == null)
        {
            return;
        }
        aryAudio[nIndex].Play();
    }
}
