﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CLoading : MonoBehaviour {

    public GameObject[] _aryDots;
    int m_nIndex = 0;

    public const float SHOW_DOT_INTERVAL = 0.5f;
    float m_fShowDotTimeLapse = 0f;

    float m_fTotalShowTimeLapse = 0f;
    public float m_fTotalShowTime = 0f;
    public bool m_bTotalShowTime = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if ( m_bTotalShowTime )
        {
            m_fTotalShowTimeLapse += Time.deltaTime;
            if (m_fTotalShowTimeLapse >= m_fTotalShowTime)
            {
                this.gameObject.SetActive(false );
            }
        }

        m_fShowDotTimeLapse += Time.deltaTime;
        if (m_fShowDotTimeLapse < SHOW_DOT_INTERVAL)
        { 
            return;  
        } 
        m_fShowDotTimeLapse = 0f;
        if (m_nIndex >= _aryDots.Length)   
        {  
            for ( int i = 0; i < _aryDots.Length; i++ )
            {
                _aryDots[i].SetActive(false); 
            }
            m_nIndex = 0;
            return;
        }

        _aryDots[m_nIndex].SetActive( true );
        m_nIndex++;
    }
}
