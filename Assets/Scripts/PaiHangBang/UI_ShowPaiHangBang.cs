﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
public class UI_ShowPaiHangBang : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    Main m_Main;
	public static bool s_bUsingUi = false;

	// Use this for initialization
	void Start ()
    {

	}
	
	// Update is called once per frame
	void Update () 
    {

	}



    public void OnPointerDown(PointerEventData evt)
	{
        CPaiHangBang_Mobile.s_Instance.ShowPaiHangBang();
    }

    public void OnPointerUp(PointerEventData evt)
    {
        CPaiHangBang_Mobile.s_Instance.HidePaiHangBang();
    }

	public void OnPointerEnter(PointerEventData evt)
	{
 
	}


	public void OnMove(PointerEventData evt)
	{
		
	}
}
