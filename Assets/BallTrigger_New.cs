﻿/*
 * 这个文件中的代码可以好好重构一次，太乱了。性能很低下
 * 
 * */


using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallTrigger_New : MonoBehaviour {

    static Vector2 vec2Temp = new Vector2();


    public Ball _ball;
	
	public Thorn _thorn;
	public eTriggerType _type;

	public enum eTriggerType
	{
		ball,
		bean,
		thorn,
		soft_obstacle,
		hard_obstacle,
        spore,
	};

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	// 接触
	void OnTriggerEnter2D(Collider2D other)
	{
        if ( CCheat.s_Instance.GetXingNengCeShi() )
        {
            return;
        }

        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game) {
			return;
		}

        // 新的吃孢子流程
        if (this._type == eTriggerType.ball && this._ball._Player.IsMainPlayer() && other.transform.gameObject.tag == "spore" )
        {
            CSpore spore = other.gameObject.GetComponent<CSpore>();
            this._ball.EatSpore(spore);
        }

       

		// 优化后的吃刺流程
		if (this._type == eTriggerType.ball && other.transform.gameObject.tag == "thorn" && this._ball._Player.IsMainPlayer ()) {
			CMonster thorn = other.transform.gameObject.GetComponent<CMonster> ();
			if (thorn) {
				this._ball.AddToEatThornList (thorn);
			}
		}

		// 优化后的星尘流程
		if (this._type == eTriggerType.ball/* && this._ball._Player.IsMainPlayer ()*/) {
            if (other.gameObject.tag == "Dust")
            {

                // 分球的壳和金壳都不与星辰碰撞
                Physics2D.IgnoreCollision(this._ball._Collider, other);
                Physics2D.IgnoreCollision(this._ball._ColliderTemp, other);


                // 没想到处于硬壳碰撞状态时，“OnTriggerEnter2D”的执行频率也挺高的。先就这样了，下阶段再想办法进一步优化.
                if (this._ball._Player.IsMainPlayer())
                {
                    CDust dust = other.gameObject.GetComponent<CDust>();
                    if (dust)
                    {
                        this._ball._Player.AddDustToSyncPool(dust);
                        this._ball.AddDust(dust);
                        dust.BeginBling();
                    }
                }

                if (this._ball.IsEjecting() && !this._ball._Player.IsSneaking() && !this._ball.IsIgnoreDust())
                {
                    this._ball.SlowDownEject();
                }
            }
		} 

		// 优化后的豆子流程
        /*
        if (this._type == eTriggerType.ball && other.transform.gameObject.tag == "BeanCollection" && this._ball._Player.IsMainPlayer()) // poppin test
        {
            if (!this._ball._Player.IsMainPlayer())
            {
                Debug.Log( "其它玩家撞到豆子了耶" );
            }
            else
            {
                CBeanCollection bl = other.transform.gameObject.GetComponent<CBeanCollection>();
                bl.ProcessBallEnter(this._ball, other);
            }
        }
        */
        if ( this._type == eTriggerType.ball && other.transform.gameObject.tag == "B" )
        {
            if (this._ball._Player.IsMainPlayer())
            {
                CBean bean = other.gameObject.GetComponent<CBean>();
                this._ball.EatBean_New(bean);
            }
            else
            {
                Physics2D.IgnoreCollision(this._ball._Collider, other );
            }
        }


        // 优化后的两个球球PK流程
        if ( this._type == eTriggerType.ball && this._ball._Player.IsMainPlayer() && other.transform.gameObject.tag == "Mouth")
        {
            Ball ballOpponent = other.transform.parent.parent.gameObject.GetComponent<Ball>();
            if (ballOpponent)
            {
                this._ball.AddToPKList(ballOpponent);

                // 分球壳只与自己队伍的球球的分球壳碰撞，不与其它玩家的球球的分球壳碰撞
				if (!Ball.IsTeammate (this._ball, ballOpponent)) {
					Physics2D.IgnoreCollision (this._ball._Collider, ballOpponent._Collider);

 
                }

                //// 不应碰撞的壳之间建立“互相忽略”关系
                // 作一些碰撞忽略操作
                Physics2D.IgnoreCollision(this._ball._ColliderDust, ballOpponent._ColliderDust);
                Physics2D.IgnoreCollision(this._ball._ColliderDust, ballOpponent._Collider);
                Physics2D.IgnoreCollision(this._ball._ColliderDust, ballOpponent._ColliderTemp);
                Physics2D.IgnoreCollision(ballOpponent._ColliderDust, this._ball._ColliderDust);
                Physics2D.IgnoreCollision(ballOpponent._ColliderDust, this._ball._Collider);
                Physics2D.IgnoreCollision(ballOpponent._ColliderDust, this._ball._ColliderTemp);


                // 金壳
                if (Ball.IsTeammate(this._ball, ballOpponent))
                {
                    Physics2D.IgnoreCollision(this._ball._ColliderTemp, ballOpponent._ColliderTemp);
                }

                
                ////
            }
        }
     


        if ( this.gameObject.tag == "thorn" )
        {
            CMonster jueshengqiu = this.gameObject.GetComponent<CMonster>();
            if (jueshengqiu)
            {
                if (jueshengqiu.m_bIsJueSheng)
                {
                    CMonster thorn = other.transform.gameObject.GetComponent<CMonster>();
                    if (thorn)
                    {
                        GameObject.Destroy( thorn.gameObject );
                    }
                }
            }
        }

        /*
		if (other.transform.gameObject.tag == "thorn") {
			CMonster thorn = other.transform.gameObject.GetComponent<CMonster> ();
			if (thorn) {
                if (thorn.GetMonsterBuildingType() != CMonsterEditor.eMonsterBuildingType.thorn)
                {
                    this._ball.EatThorn(thorn);
                }
			}
		} else if (other.transform.gameObject.tag == "bean") {
			CMonster bean = other.transform.gameObject.GetComponent<CMonster> ();
			if (bean) {
				this._ball.EatBean (bean);
			}
		} else if (other.transform.gameObject.tag == "Grass") {
			CGrass grass = other.transform.gameObject.GetComponent<CGrass> ();
			this._ball.EnterGrass (grass);
		} 
		*/
    }

    /*
    void OnTriggerStay2D(Collider2D other)
    {

    }
    */

	void OnTriggerExit2D(Collider2D other)
	{
        if (AccountManager.m_eSceneMode != AccountManager.eSceneMode.Game)
		{
			return;
		}

		if (this._type != eTriggerType.ball)
		{
			return;
		}

		/*
	    if (other.transform.gameObject.tag == "Grass") {
			CGrass grass = other.transform.gameObject.GetComponent<CGrass> ();
			this._ball.LeaveGrass ();
		}
		*/

        if (this._type == eTriggerType.ball && other.transform.gameObject.tag == "BeanCollection" && this._ball._Player.IsMainPlayer())
        {
            CBeanCollection bl = other.transform.gameObject.GetComponent<CBeanCollection>();
            bl.ProcessBallExit(this._ball);
        }

        // 两个球球PK
		if (this._type == eTriggerType.ball && this._ball._Player.IsMainPlayer() && other.transform.gameObject.tag == "Mouth")
        {
            Ball ballOpponent = other.transform.parent.parent.gameObject.GetComponent<Ball>();
            if (ballOpponent)
            {
                this._ball.RemoveFromPKList(ballOpponent);
            }
        }

		// 优化后的星尘流程
		if (this._type == eTriggerType.ball && this._ball._Player.IsMainPlayer () && other.gameObject.tag == "Dust") {
			// 没想到处于硬壳碰撞状态时，“OnTriggerEnter2D”的执行频率也挺高的。先就这样了，下阶段再想办法优化.
			CDust dust = other.gameObject.GetComponent<CDust> ();
			if (dust) {
				this._ball._Player.RemoveDustFromSyncPool (dust);
                this._ball.RemoveDust(dust);
            }
		} 

		// 优化后的吃刺流程
		if ( this._type == eTriggerType.ball && other.transform.gameObject.tag == "thorn" && this._ball._Player.IsMainPlayer())
		{
			CMonster thorn = other.transform.gameObject.GetComponent<CMonster>();
			if (thorn)
			{
				this._ball.RemoveFromEatThornList(thorn);
			}
		}
    }
}
